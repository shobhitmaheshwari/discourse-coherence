package nlp.coherence;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLPClient;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;

public class App {

	private static final String[] transitionCats = new String[] { "SS", "SO", "SX", "S-", "OS", "OO", "OX", "O-", "XS",
			"XO", "XX", "X-", "-S", "-O", "-X", "--" };

	private static final ImmutableMap<Integer, String> syntacticCategoryMap = new ImmutableMap.Builder<Integer, String>()
			.put(0, "-").put(1, "X").put(2, "O").put(3, "S").build();

	private static final ObjectMapper mapper = new ObjectMapper();

	public static void main(String[] args) throws Exception {

		Properties appProps = new Properties();
		appProps.load(new FileReader("app.properties"));
		
		Instant t1 = Instant.now();
		Path dataPath = FileSystems.getDefault().getPath(appProps.getProperty("data.path"));
		int numNLPThreads = Integer.parseInt(appProps.getProperty("nlp.threads"));
		if (numNLPThreads < 1 || numNLPThreads > 8) {
			throw new Exception("Invalid # of threads.  Pick value between 1-8");
		}
		String outputFilePrefix = appProps.getProperty("data.output_path") + appProps.getProperty("data.output_prefix");
		boolean negativeSampling = Boolean.valueOf(appProps.getProperty("nlp.neg_sample"));
		int index = 1;
		for (Path file : Files.newDirectoryStream(dataPath)) {
			System.out.println("Processing " + file.getFileName());
			try {
				process(file.toFile(), new File(outputFilePrefix + index + ".json"), negativeSampling, numNLPThreads);
			}catch(Exception e) {
				e.printStackTrace();
			}
			index++;
		}


	}

	/**
	 * Converts blogposts into entity grids and feature vectors, then saves the
	 * latter to a .json file
	 * 
	 * @param inputFile
	 * @param outputFile
	 * @throws Exception
	 */
	private static void process(File inputFile, File outputFile, boolean negativeSampling, int numThreads) throws Exception {

		Map<String, Annotation> posts = getPosts(inputFile);
		
		StanfordCoreNLPClient nlpClient = new StanfordCoreNLPClient(
				PropertiesUtils.asProperties("annotators", "tokenize, ssplit, pos", "outputFormat", "json"),
				"http://localhost", 9000, numThreads);
		nlpClient.annotate(posts.values(), numThreads);
		System.out.println("Annotating...");
		nlpClient.shutdown();
		
		System.out.println("Generating Entity Grids...");
		Map<String, int[][]> entityGrids = new HashMap<String, int[][]>();
		for (String postId : posts.keySet()) {
			int[][] entityGrid = generateEntityGrid(posts.get(postId), negativeSampling);
			if(entityGrid != null) {
				entityGrids.put(postId, entityGrid);
			}
		}

		System.out.println("Generating Feature Vectors...");
		List<Map<String, ? extends Number>> featureVectors = new ArrayList<Map<String, ? extends Number>>();
		for (String postId : entityGrids.keySet()) {
			Map<String, ? extends Number> featureVector = generateFeatureVector(postId, entityGrids.get(postId));
			//filter feature vecs with no first/second-person transitions
			if (featureVector.get("--").doubleValue() != 1.0) {
				featureVectors.add(featureVector);
			}
		}
		
		System.out.println("Saving Output...");
		saveJSONToFile(outputFile, featureVectors);
	}

	/**
	 * Saves feature vectors to .json file
	 * 
	 * @param file
	 * @param mapper
	 * @param json
	 */
	private static void saveJSONToFile(File file, List<Map<String, ? extends Number>> json) {
		try {
			mapper.writeValue(file, json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Retrieves posts from CSV file
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	private static Map<String, Annotation> getPosts(File file) throws Exception {
		Map<String, Annotation> posts = new HashMap<String, Annotation>();
		if (file.getName().endsWith(".csv")) {
			Reader in = new FileReader(file);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
			for (CSVRecord record : records) {
				posts.put(record.get("post_id"), new Annotation(record.get("content")));
			}
		} else {
			throw new Exception("File type not valid");
		}
		return posts;

	}

	/**
	 * Generates entity grid from post
	 * 
	 * @param post
	 * @return
	 */
	private static int[][] generateEntityGrid(Annotation post, boolean negativeSampling) {
		
		List<CoreMap> sentences = post.get(SentencesAnnotation.class);
		if(negativeSampling) {
			Collections.shuffle(sentences, new Random(System.nanoTime()));
		}
		
		int[][] entityGrid = new int[2][sentences.size()];
		int i = 0;
		boolean validPost = false;
		for (CoreMap sentence : sentences) {
			List<CoreLabel> sentenceTokens = sentence.get(TokensAnnotation.class);
			int j = 0;
			if (sentenceTokens.size() > 5 && sentenceTokens.size() < 20) {
				validPost = true;
				for (CoreLabel token : sentenceTokens) {
					String word = token.getString(TextAnnotation.class);

					// Identify first/second person subjects
					if (entityGrid[0][i] < 3 && word.matches("[iI]('m)?")) {
						entityGrid[0][i] = 3;
					} else if (word.matches("[wW]e('re)?")) {
						if ((entityGrid[0][i] < 3)) {
							entityGrid[0][i] = 3;
						}
						if ((entityGrid[1][i] < 3)) {
							entityGrid[1][i] = 3;
						}
					} else if (entityGrid[1][i] < 3 && word.matches("[yY]ou")) {
						if (j < sentenceTokens.size() - 1
								&& sentenceTokens.get(j + 1).getString(PartOfSpeechAnnotation.class).matches("VB*")) {
							entityGrid[1][i] = 3;
						}
					} else if (entityGrid[1][i] < 3 && word.matches("[yY]ou're")) {
						entityGrid[1][i] = 3;
					}

					// Identify first/second person objects
					else if (entityGrid[0][i] < 2 && word.matches("(me|myself)")) {
						if (j - 1 >= 0
								&& sentenceTokens.get(j - 1).getString(PartOfSpeechAnnotation.class).matches("VB*")) {
							entityGrid[0][i] = 2;
						}
					} else if (word.matches("(us|ourselves)")) {
						if (j - 1 >= 0
								&& sentenceTokens.get(j - 1).getString(PartOfSpeechAnnotation.class).matches("VB*")) {
							if (entityGrid[0][i] < 2) {
								entityGrid[0][i] = 2;
							}
							if (entityGrid[0][i] < 2) {
								entityGrid[1][i] = 2;
							}
						}
					} else if (entityGrid[1][i] < 2 && word.matches("(you|yourself)")) {
						if (j - 1 >= 0
								&& sentenceTokens.get(j - 1).getString(PartOfSpeechAnnotation.class).matches("VB*")) {
							entityGrid[1][i] = 2;
						}
					}

					// Identify other first/second person references
					else if (entityGrid[0][i] < 1 && word.matches("[Mm]y")) {
						entityGrid[0][i] = 1;
					} else if (entityGrid[1][i] < 1 && word.matches("[Yy]our")) {
						entityGrid[1][i] = 1;
					} else if (word.matches("[Oo]ur")) {
						if (entityGrid[0][i] < 1) {
							entityGrid[0][i] = 1;
						}
						if (entityGrid[1][i] < 1) {
							entityGrid[1][i] = 1;
						}
					}

					j++;
				}
				i++;
			}
		}
		
		if(!validPost) {
			entityGrid = null;
		}
		return entityGrid;
	}

	/**
	 * Generates feature vector from entity grid
	 * 
	 * @param postId
	 * @param entityGrid
	 * @return
	 */
	private static Map<String, Number> generateFeatureVector(String postId, int[][] entityGrid) {
		double totalTransitions = 2 * (entityGrid[0].length - 1);
		Map<String, Number> featureVector = new HashMap<String, Number>();
		featureVector.put("postId", Integer.parseInt(postId));
		for (int[] row : entityGrid) {
			for (int i = 0; i < row.length - 1; i++) {
				String transition = syntacticCategoryMap.get(row[i]) + syntacticCategoryMap.get(row[i + 1]);
				featureVector.compute(transition, (x, y) -> y != null ? y.intValue() + 1 : 1);
			}
		}
		for (String cat : transitionCats) {
			featureVector.compute(cat,
					(x, y) -> totalTransitions > 0.0 && y != null ? (y.doubleValue() / totalTransitions) : 0.0);
		}
		return featureVector;
	}
}
