import nltk
import gensim
import csv
import pickle
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models

tagged_blogs = []

def lda_tagging():
	en_stopwords = get_stop_words('en')
	en_stopwords = en_stopwords + [".", ",", "!", "?", ":", ";", "'s", "``", "''", "n't", ")", "(",
		"%", "'t", "'d", "I", "Mr", "..."]
	stemmer = PorterStemmer()
	texts = []

	blogs1 = csv.DictReader(open("pandas1.csv"))
	for row in blogs1:
		tokens = nltk.word_tokenize(row['content'])
		words = []
		for token in tokens:
			words.append(token.lower())
		normal = [i for i in words if not i in en_stopwords]
		try:
			text = [stemmer.stem(i) for i in normal]
			texts.append(text)
		except:
			print("Bad blog!")

	blogs2 = csv.DictReader(open("pandas2.csv"))
	for row in blogs2:
		tokens = nltk.word_tokenize(row['content'])
		words = []
		for token in tokens:
			words.append(token.lower())
		normal = [i for i in words if not i in en_stopwords]
		try:
			text = [stemmer.stem(i) for i in normal]
			texts.append(text)
		except:
			print("Bad blog!")

	print("text normalized")

	dictionary = corpora.Dictionary(texts)
	corpus = [dictionary.doc2bow(text) for text in texts]

	dictionary.save('blogs.dict')
	corpora.MmCorpus.serialize('blogcorpus.mm', corpus)

	print("corpus complete")

	# ldamodel = models.ldamodel.LdaModel(corpus, num_topics=300, id2word=dictionary, passes=20)
	ldamodel = models.LdaMulticore(corpus, num_topics=200, id2word=dictionary, passes=20, workers=3)

	f = open('ldamodel.pickle', 'wb')
	pickle.dump(ldamodel, f)
	f.close()

lda_tagging()