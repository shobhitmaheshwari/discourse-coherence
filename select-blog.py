#necessary pickle files and dict file are on nldslab, user cmps143a3, folder /lda

import nltk, gensim, pickle
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models

#utterance input is a single utterance
def blog_selection(utterance):
	f = open('topic2doc.pickle', 'rb')
	topic_2_docid = pickle.load(f)
	f.close()
	f = open('ldamodel.pickle', 'rb')
	ldamodel = pickle.load(f)
	f.close()

	dictionary = corpora.Dictionary.load('blogs.dict')

	lda_topics = ldamodel[dictionary.doc2bow(utterance.split())]
	lda_topics_sorted = sorted(lda_topics, key=lambda tup: tup[1], reverse=True)
	# print(lda_topics_sorted[:3])

	docs0 = topic_2_docid[lda_topics_sorted[0][0]]
	docs1 = topic_2_docid[lda_topics_sorted[1][0]]
	docs2 = topic_2_docid[lda_topics_sorted[2][0]]

	seen1 = docs1
	seen2 = []
	seen3 = []
	for doc_id in seen1:
		if doc_id in seen1:
			seen2.append(doc_id)
		else:
			seen1.append(doc_id)
	for doc_id in docs2:
		if doc_id in seen2:
			seen3.append(doc_id)
	if len(seen3) > 0:
		# print("yay!")
		return seen3
	else:
		return seen2

#Utterance in given as a string to the function
#Uncomment the following two lines and/or other print statements to see outputs:
# chosen = blog_selection("I like to watch movies.")
# print(chosen)