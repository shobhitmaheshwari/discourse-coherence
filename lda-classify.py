# TODO:
# Only tag the top 3 topics for each document, link utterance to documents with same top 3
# Find a way to store document clusters
# create topic to document id graph (pickle file)
# sql document storage (mysql is top choice)

import nltk
import gensim
import csv
import pickle
import sys
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models

def lda_tagging(infile, outfile, ldamodel):
	en_stopwords = get_stop_words('en')
	en_stopwords = en_stopwords + [".", ",", "!", "?", ":", ";", "'s", "``", "''", "n't", ")", "(",
		"%", "'t", "'d"]
	stemmer = PorterStemmer()
	texts = []
	rows = []

	blogs = csv.DictReader(open(infile))
	headers = blogs.fieldnames
	headers.append("lda_tags")
	# labeled_blogs = csv.DictWriter(open(outfile, 'w'), fieldnames=fieldnames)
	for row in blogs:
		tokens = nltk.word_tokenize(row['content'])
		words = []
		for token in tokens:
			words.append(token.lower())
		normal = [i for i in words if not i in en_stopwords]
		try:
			text = [stemmer.stem(i) for i in normal]
			texts.append(text)
			rows.append(row)
		except:
			print("Bad blog!")
		# bow = BagOfWords(text)
		# lda_topics = ldamodel.get_document_topics(bow, minimum_probability=None, minimum_phi_value=None,
		# 	per_word_topics=True)
		# print(lda_topics)

	print("text normalized")

	dictionary = corpora.Dictionary(texts)
	corpus = [dictionary.doc2bow(text) for text in texts]

	f = open('topic2doc.pickle', 'rb')
	topic_2_docid = pickle.load(f)
	f.close()
	# topic_2_docid = {}

	labeled_blogs = csv.DictWriter(open(outfile, 'w'), fieldnames=headers)
	labeled_blogs.writeheader()
	for num, text in enumerate(texts):
		try:
			row = rows[num]
			lda_topics = ldamodel[dictionary.doc2bow(text)]
			lda_topics_sorted = sorted(lda_topics, key=lambda tup: tup[1], reverse=True)
			lda_topics_sorted = lda_topics_sorted[:3]
			# TODO: only top 3 topics
			row["lda_tags"] = lda_topics_sorted
			labeled_blogs.writerow(row)
			for topic in lda_topics_sorted:
				if topic[0] not in topic_2_docid:
					topic_2_docid[topic[0]] = []
				topic_2_docid[topic[0]].append(row['post_id'])
		except:
			print("failed to add text", num)
			break

	f = open('topic2doc.pickle', 'wb')
	pickle.dump(topic_2_docid, f)
	f.close()
	print("topic2doc updated")

if __name__ == "__main__":
	print(sys.argv[1])
	infile  = "pandas" + str(sys.argv[1]) + ".csv"
	outfile = "pandas" + str(sys.argv[1]) + "-lda.csv"
	f = open('ldamodel.pickle', 'rb')
	ldamodel = pickle.load(f)
	f.close()

	# print(ldamodel.print_topics(num_topics=2, num_words=4))
	lda_tagging(infile, outfile, ldamodel)